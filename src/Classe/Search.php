<?php

namespace App\Classe;

use App\Entity\Categories;

class Search
{
    /**
     * @var string
     */
    public $string = '';
    /**
     * @var Categories[]
     */
    public $minprice = null;
    public $maxprice = null;
    public $tags = null;
    public $categories = [];
}
