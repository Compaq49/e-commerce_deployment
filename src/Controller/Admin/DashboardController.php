<?php

namespace App\Controller\Admin;

use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\Carrier;
use App\Entity\Product;
use App\Entity\Categories;
use App\Controller\Admin\OrderCrudController;
use App\Entity\Contact;
use App\Entity\HomeSlider;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        //return parent::index();
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(OrderCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
        ->setTitle('<img src="assets/images/clown.jpg" width="45px" height="45px">&nbsp; E-commerce.');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Back to Home Page.', 'fas fa-home', 'home');
        yield MenuItem::linkToCrud('Products', 'fas fa-shopping-cart', Product::class);
        yield MenuItem::linkToCrud('Orders', 'fa fa-shopping-bag', Order::class);
        yield MenuItem::linkToCrud('Carts', 'fa fa-boxes', Cart::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-list', Categories::class);
        yield MenuItem::linkToCrud('Carrier', 'fa fa-truck', Carrier::class);
        yield MenuItem::linkToCrud('Home Slider', 'fa fa-images', HomeSlider::class);
        yield MenuItem::linkToCrud('Contact', 'fa fa-envelope', Contact::class);
    }
    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
                ->setName($user->getFullName())
                ->setGravatarEmail($user->getUserIdentifier())
                ->displayUserAvatar(true);
    }
}
